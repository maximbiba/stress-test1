package main.com.slice.sortarray;

public class SortArray {

    public int[] sortArray(int[] array) {
        boolean needIteration = true;
        while(needIteration){
            needIteration = false;
            for (int i = 1; i < array.length; i++){
                if (array[i] < array[i-1]){
                    swap(array, i, i-1);
                    needIteration = true;
                }
            }
        }
        return array;
    }

    private void swap (int[] a, int ind1, int ind2) {
        int tmp = a[ind1];
        a[ind1] = a[ind2];
        a[ind2] = tmp;
    }

}
